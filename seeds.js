var mongoose = require("mongoose");
var Poll = require("./models/poll");
var SportsEvent = require("./models/sportsEvent");
var fs = require("fs");

var sportsEventData = [];

fs.readFile("./sports-events.json", "utf-8", function(err, data){
    
    if(err){
        console.log(err);
    } else {
        sportsEventData = JSON.parse(data);
    }
});

var pollData = [
    {
    eventId: "1002916450",
    userId: "wef35445",
    vote: "Away Team win",
    dateTime: new Date().toISOString()
    },
    {
    eventId: "1002916451",
    userId: "wef35490",
    vote: "Away Team win",
    dateTime: new Date().toISOString()
    },
    {
    eventId: "1003022923",
    userId: "wef35467",
    vote: "Home Team win",
    dateTime: new Date().toISOString()
    }
];

/*var sportsEventData = [
    {
        awayName: "Panthrakikos Komotini",
        createdAt: "2015-12-18T12:30:39.228Z",
        group: "Greek Cup",
        homeName: "Chania FC",
        id: 1002916450,
        name: "Chania FC - Panthrakikos Komotini",
        sport: "FOOTBALL",
        country: "ENGLAND",
        state: "STARTED"
    },
    {
        awayName: "Njoze, M",
        createdAt: "2015-12-18T12:30:39.322Z",
        group: "El Kantaoui",
        homeName: "Stoilkovska, M",
        id: 1003026214,
        name: "Stoilkovska, M - Njoze, M",
        sport: "TENNIS",
        country: "SWEDEN",
        state: "STARTED"
    }
    
    ];*/

function seedDB(){
    /*Poll.remove({}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("cleared Polls db");
            //add some dummy polls
            pollData.forEach(function(seed){
                Poll.create(seed, function(err, data){
                    if(err){
                        console.log(err);
                    } else {
                        console.log("a poll has been added");
                    }
                });
            });
        }
    });*/
    
    SportsEvent.remove({}, function(err){
        if(err){
            console.log(err);
        } else {
            //console.log("cleared sports events db");
            //add some dummy polls
            var n = 1;
            sportsEventData.forEach(function(seed){
                SportsEvent.create(seed, function(err, data){
                    if(err){
                        console.log(err);
                    } else {
                       // console.log("Sports events added: " + n++);
                    }
                });
            });
        }
    });
}

module.exports = seedDB;
