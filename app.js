var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var flash = require("connect-flash");


//var Poll = require("./models/poll");
//var SportsEvent = require("./models/sportsEvent");
var User = require("./models/user");
var seedDB = require("./seeds");

//requiring routes (so that express router.get, ... can be used inplace of app.get, ... for all routes)
var indexRoutes = require("./routes/index");
var pollRoutes = require("./routes/polls");
var eventRoutes = require("./routes/sportsevents");

//Some app Config
//mongoose.connect("mongodb://localhost/sports_pollv2");

var url = process.env.SPDATABASEURL || "mongodb://localhost/sports_pollv2";
mongoose.connect(url);

//console.log(process.env.SPDATABASEURL);

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(flash());

//seed the sports events db with data fro json file
seedDB();


//Passport Config
app.use(require("express-session")({
    secret: "coding is the access to infinite intelligence",
    resave:false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//this middleware is used to parse user obj to all template/routes
app.use(function(req, res, next){
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    res.locals.info = req.flash("info");
    next();
});

//in orderto use express router in all routes
app.use(indexRoutes);
app.use(pollRoutes);
app.use(eventRoutes);

//start server
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("The Sports Poll Server Has Started!");
});

module.exports = app;
