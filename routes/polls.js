var express = require("express");
var router = express.Router({mergeParams: true}); // access to params can be improved using "var router = express.Router({mergeParams: true});""
var controller = require("../controllers/controller");
var middleware = require("../functions/middleware");


//Polls route
router.get("/polls", controller.getPolls);

//Voting route
router.post("/polls", middleware.isLoggedIn, controller.postPoll);

//new random vote route
router.get("/polls/new", middleware.isLoggedIn, controller.getRandPoll);

module.exports = router;