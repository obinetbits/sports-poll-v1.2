var express = require("express");
var router = express.Router({mergeParams: true});
var passport = require("passport");
var User = require("../models/user");

//landing page route
router.get("/", function(req, res){
    res.redirect("/sportsevents");
});


//******************
//Auth routes
//******************

//render sign up form
router.get("/signup", function(req, res){
    res.render("signup");
});

//handle signup form submit
router.post("/signup", function(req, res){
    //reset "session"
    req.session.vFlag ="";
    req.session.unVotedEventsId = "";
    
    var newUser = new User({username: req.body.username}); 
    User.register(newUser, req.body.password, function(err, user){
        if(err){
            console.log(err);
            req.flash("error", err.message);
            return res.render("signup");
        }
        passport.authenticate("local")(req, res, function(){
            req.flash("success", "Welcome to Sports Poll " + user.username);
            res.redirect("/sportsevents");
        });
    });
});

//show login form
router.get("/login", function(req, res){
    //reset "session"
    req.session.vFlag ="";
    req.session.unVotedEventsId = "";
    
    res.render("login");
});

//Handle login logic
router.post("/login", passport.authenticate("local", 
    {
        successRedirect: "/sportsevents",
        failureRedirect: "/login",
        successFlash: "Welcome Back"
    }), function(req, res){

});

//handle logout logic
router.get("/logout", function(req, res){
    
    //reset "session"
    req.session.vFlag ="";
    req.session.unVotedEventsId = "";
    
    req.logout();
    
    req.flash("success", "Logged you out");
    res.redirect("/sportsevents");
});


module.exports = router;