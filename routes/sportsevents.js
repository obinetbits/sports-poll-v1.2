var express = require("express");
var router = express.Router(); // access to params can be improved using "var router = express.Router({mergeParams: true});""

var controller = require("../controllers/controller");

//Sports Events route
router.get("/sportsevents", controller.getSportsEvents);

module.exports = router;