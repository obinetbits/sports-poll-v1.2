var mongoose = require("mongoose");

//Schema setup
var pollSchema = new mongoose.Schema({
    eventId: Number,
    voter: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        votername: String
    },
    vote: String,
    dateTime: {
                type: Date,
                default: new Date().toISOString()
              }
});

module.exports = mongoose.model("Poll", pollSchema);
