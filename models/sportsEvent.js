var mongoose = require("mongoose");

//Schema setup
var SportsEventSchema = new mongoose.Schema({
    awayName: String,
    createdAt: String,
    group: String,
    homeName: String,
    id: Number,
    name: String,
    sport: String,
    country: String,
    state: String
});

module.exports = mongoose.model("SportsEvent", SportsEventSchema);
