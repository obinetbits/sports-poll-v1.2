# **Sports Poll App overview** #

#To start app
-node app.js

#To run test
-npm test

#When sports poll app is started
* -app loads sports event to db.
* -opens app's landing page which lists all sports events.

#When user clicks the *start voting* button on landing page
* -it checks if user is logged in
* -if user is logged in, it retrieves all sports events user have not voted on into an array which would be valid for current user session
* -it selects a random sports event and renders new vote with "STARTED" state to the user
* -if user is not logged in it renders login page.

#When user click *vote* button on voting page
* -app creates a new vote row in the polls db and then renders new random vote to the user
* -when ever the use clicks *vote* button, app keeps rendering new random vote from users unvoted list until user has no more unvoted events for that session then app redirects user to landing page.
* -if user clicks on *start voting* button on the landing page after exhausting unvoted events list, app renders landing page and tells the user about the issue
* -if user have skipped any voting it will become available on next user login session

#When user submits a login form
* -app checks if user is registered
* -if user is registered, it renders the landing page and keeps track of session with a session flag
* -if submitted data is not valid, it re-renders the login form 

#When user submits a signup form
* -if user data is valid it registers user and re-directs to landing page. It also keeps track of session
* -if user data is not valid it re-renders signup form

#When user logs out
* -app erases session flags




                                      Tools Used

#dependencies:
* *body-parser1.15.2
* *connect-flash0.1.1
* *ejs2.5.1
* *express4.14.0
* *express-session1.14.1
* *lodash4.15.0
* *mongoose4.5.10
* *passport0.3.2
* *passport-local1.0.0
* *passport-local-mongoose: "^4.0.0
 
#devDependencies: 
* *chai3.5.0
* *mocha3.0.2
* *request2.74.0
* *supertest2.0.0