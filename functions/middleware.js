//middleware for checking user authentication
module.exports.isLoggedIn = function (req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "Login is required");
    res.redirect("/login");
};