var SportsEvent = require("../models/sportsEvent");
var arrayDiff = require('lodash/array'); //geting difference btw two arrays

var handler = {
    //Gets unvoted sports events and calls the randomEventsHandler to render poll to current user
    unVotedEventsHandler: function(req, res, votedEventsId, unVotedEventsIds){
        SportsEvent.distinct("id", {state: "STARTED"}, function(err, sportsevents){
            if(err){
                console.log(err);
            } else {
                var allSportsEventsId = [];
                var newSportsEventsArr = [];
                for(var i = 0; i < sportsevents.length; i++){
                    allSportsEventsId.push(sportsevents[i]);
                }
                //console.log("allSportsEventsId:" + allSportsEventsId.length);
                                    
                //get the event ids that the current user has not voted on. 
                //i.e the difference btw allSportsEventsId & votedEventsId
                //the result will be used to render events to be voted on for current user
                newSportsEventsArr = arrayDiff.difference(allSportsEventsId, votedEventsId);
                //console.log("newSportsEventsArr:" + newSportsEventsArr.length);
                                        
                //add event ids to be voted on to a session variable
                req.session.unVotedEventsId = newSportsEventsArr;
                //console.log("req.session.unVotedEventsId 1:" + req.session.unVotedEventsId.length);
                                        
                //get a random sports event and render to current user
                handler.randomEventsHandler(req, res, req.session.unVotedEventsId);
            }
        }); 
    },
    
    //gets a random sports event and render to current user
    randomEventsHandler: function(req, res, unVotedEventsIds){
        
        //console.log("unVotedEventsIds 2:" + unVotedEventsIds.length);
           
        var shuffleResult = shuffle(unVotedEventsIds);
                
        var ranId = shuffleResult.next().value;
        if(ranId !== undefined){
            //get a sports event from db
            SportsEvent.findOne({"id": ranId}, function(err, sportsEvent){
                if(err){
                    console.log(err);
                } else {
                    res.render("new", {sportsEvent: sportsEvent});
                    //console.log(sportsEvent);
                }
            });
        } else {
            //response when there is no more valid events to vote on for current session
            req.flash("info", "No more votes available for you at the moment. If you have skipped any votes, they will be available on your next login");
            res.redirect("/sportsevents");
        }
    }
};

module.exports = handler;



//function for shuffling array of numbers
function* shuffle(numArray) {

    var i = numArray.length;

    while (i--) {
        yield numArray.splice(Math.floor(Math.random() * (i+1)), 1)[0];
    }

}