var SportsEvent = require("../models/sportsEvent");
var Poll = require("../models/poll");
var handler = require("../functions/index");

var controller = {
    getPolls: function(req, res){
        //get all polls/votes from the db
        /*Poll.find({}, function(err, polls){
            if(err){
                console.log(err);
            } else {
                //res.render("polls", {polls: polls});
                res.json({polls: polls});
            }
        });*/ //when u uncomment this db request, remove the line res.redirect("/sportsevents");
    res.redirect("/sportsevents");
    },
    
    postPoll: function(req, res){
        //create a new vote in the db
        
        //get vote data from a current user
        var eventId = req.body.eventId;
        var voter = {id: req.user._id, votername: req.user.username};
        var vote = req.body.vote;
        //var dateTime = new Date().toISOString();
        //var newPoll = {eventId: eventId, voter: voter, vote: vote, dateTime: dateTime};
        var newPoll = {eventId: eventId, voter: voter, vote: vote};
        
        //Create a new vote in the polls db
        Poll.create(newPoll, function(err, poll){
            if(err){
                console.log(err);
            } else {
                //console.log("Newly created vote");
                //console.log(poll);
            }
        });
        
        //render new vote page to the user after each vote
        res.redirect("/polls/new");
    },
    
    getRandPoll: function(req, res){
        //get a random vote to render to user
        //response to render a vote on first request after login
        if(req.session.vFlag !== 1){ 
            //vFlag is used to recorganise first vote after new login session
            req.session.vFlag = 1;
            
        //get all event ids from polls db where current user have voted
            Poll.find({"voter.votername": req.user.username},{ eventId: 1, _id: 0 }, function(err, polls){
                if(err){
                    console.log(err);
                } else {
                    var votedEventsId = []; 
                    for(var i = 0; i < polls.length; i++){
                        votedEventsId.push(polls[i].eventId);
                    }
                        //console.log("votedEventsId:" + votedEventsId.length);
                            
                        //get all ids from events db with the state "STARTED"
                        handler.unVotedEventsHandler(req, res, votedEventsId);
                }
            });
        } else {
            //get a random sports event and render to current user
            handler.randomEventsHandler(req, res, req.session.unVotedEventsId);
        }
    },
    
    getSportsEvents: function(req, res){
        //get all sports events from db
        SportsEvent.find({}, function(err, sportsevents){
            if(err){
                console.log(err);
            } else {
                res.render("sportsevents", {sportsevents: sportsevents});
            }
        });
    }
    
};

module.exports = controller;