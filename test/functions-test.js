var chai = require("chai"),
    expect = chai.expect,
    assert = chai.assert;
    chai.use(require("chai-sorted"));

    
var sinon = require('sinon');
//var assert = require('assert');

var arrayDiff = require('lodash/array'); //geting difference btw two arrays
var handler = require("../functions/index");

var mongoose = require('mongoose');

//import sample data
var sampleData = require('./sample_data');


describe('Functions', function(){
    describe('arrayDiff', function() {
        
        it("should return difference between two arrays", function(done){
            var allSportsEventsIds = [1003026299,1003026214,1003026673,1003026234];
            var votedSportsEventsIds = [1003026299,1003026214];
            var unVotedSportsEventIds = arrayDiff.difference(allSportsEventsIds, votedSportsEventsIds);
            
            expect(unVotedSportsEventIds).to.not.include.members(votedSportsEventsIds);
            done();
        });
    });
    
    describe('shuffle', function() {
        it("should shuffle array and return one random number - no repeat", function(done) {
            var unVotedSportsEventIds = [1003026214,1003026234,1003026299,1003026673];
            var emptyArr = [];
            function* shuffle(numArray) {
                var i = numArray.length;
                while (i--) {
                    yield numArray.splice(Math.floor(Math.random() * (i+1)), 1)[0];
                }
            }
            //var shuffleResult = shuffle(unVotedSportsEventIds);
            
            expect(unVotedSportsEventIds).to.be.sorted();//test that array is sorted
            expect(shuffle(unVotedSportsEventIds)).to.be.sorted(false);//test that array is not sorted after shuffle
            expect(shuffle(unVotedSportsEventIds).next().value).to.not.be.oneOf(unVotedSportsEventIds); //test that no-repeat selection occurs
            expect(shuffle(emptyArr).next().value).to.be.undefined; //test that empty array returns undefined
            done();
        });
    });
    
    describe('unVotedEventsHandler gets all unVotedSportsEventIds and parse to randomEventsHandler', function() {
        it('should exist', function(){
            expect(handler.unVotedEventsHandler).to.exist;
        });
        
        it("should get all sports events ids with state STARTED", function(done){
            var SportsEvent = mongoose.model('SportsEvent');
            var SportsEventMock = sinon.mock(SportsEvent);
            var expectedResult = sampleData.sportsEventsIds();
            SportsEventMock
                .expects('distinct').withArgs("id", {state: "STARTED"})
                .yields(null, expectedResult);

            SportsEvent.distinct("id", {state: "STARTED"}, function (err, result) {
                if(err) throw err;
                SportsEventMock.verify();
                SportsEventMock.restore();
                assert.equal(result, expectedResult);
                done();
            }); 
        });
        
        it('should call randomEventsHandler');
    });
    
    describe('randomEventsHandler renders random vote to user', function() {
        it('should exist', function(){
            expect(handler.randomEventsHandler).to.exist;
        });
        
        it('should find a sports event and render to vote page', function() {
            var SportsEvent = mongoose.model('SportsEvent');
            var SportsEventMock = sinon.mock(SportsEvent);
            var ranId = 1003015197;
            var expectedResult = sampleData.sportsEvent();
            
            SportsEventMock
                .expects('findOne').withArgs({"id": ranId})
                .yields(null, expectedResult);
                
            var req = {params: {}, session: {unVotedEventsId: [1003015197]}};
            var res = {render: sinon.stub()};
            
            handler.randomEventsHandler(req, res, req.session.unVotedEventsId);
            
            SportsEventMock.verify();
            SportsEventMock.restore();
            
            sinon.assert.calledWith(res.render, "new", {sportsEvent: expectedResult});
        });
        
        it('should call shuffle and select a random id from unVotedEventsIds');
    });
    
});