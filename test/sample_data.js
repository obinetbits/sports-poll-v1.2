module.exports.sportsEventsData = function() {
  return [
            {
                "awayName": "AS de la Marsa",
                "createdAt": "2015-12-18T12:30:39.255Z",
                "group": "Ligue 1",
                "homeName": "Club Africain",
                "id": 1003015197,
                "name": "Club Africain - AS de la Marsa",
                "objectId": "sFjPkmljKv",
                "sport": "FOOTBALL",
                "country": "ENGLAND",
                "state": "STARTED"
            },
            {
                "awayName": "Kastamonuspor",
                "createdAt": "2015-12-18T12:30:39.261Z",
                "group": "T\u00fcrkyie Kupasi",
                "homeName": "Kar\u015f\u0131yaka",
                "id": 1003016331,
                "name": "Kar\u015f\u0131yaka - Kastamonuspor",
                "objectId": "cRqV2RTmsu",
                "sport": "FOOTBALL",
                "country": "FRANCE",
                "state": "FINISHED"
            },
            {
                "awayName": "Allen, Gareth",
                "createdAt": "2015-12-18T12:30:39.266Z",
                "group": "German Masters Qualifiers",
                "homeName": "Ding Junhui",
                "id": 1003018193,
                "name": "Ding Junhui - Allen, Gareth",
                "objectId": "nPuz011p0W",
                "sport": "SNOOKER",
                "country": "SWEDEN",
                "state": "NOT_STARTED"
            },
            {
                "awayName": "Lines, Peter",
                "createdAt": "2015-12-18T12:30:39.272Z",
                "group": "German Masters Qualifiers",
                "homeName": "Trump, Judd",
                "id": 1003018186,
                "name": "Trump, Judd - Lines, Peter",
                "objectId": "CSJn3kZhdx",
                "sport": "SNOOKER",
                "country": "ENGLAND",
                "state": "NOT_STARTED"
            },
            {
                "awayName": "SKIF-Krasnodar",
                "createdAt": "2015-12-18T12:30:39.278Z",
                "group": "Cup",
                "homeName": "Dinamo Astrakhan",
                "id": 1003027200,
                "name": "Dinamo Astrakhan - SKIF-Krasnodar",
                "objectId": "enCbqOuRLr",
                "sport": "HANDBALL",
                "country": "SWEDEN",
                "state": "STARTED"
            }
        ];
};

//Sports Events with the state "STARTED"
module.exports.sportsEventsIds = function() {
  return [1003027200, 1003015197];
};

module.exports.sportsEvent = function() {
  return [
      {
                "awayName": "AS de la Marsa",
                "createdAt": "2015-12-18T12:30:39.255Z",
                "group": "Ligue 1",
                "homeName": "Club Africain",
                "id": 1003015197,
                "name": "Club Africain - AS de la Marsa",
                "objectId": "sFjPkmljKv",
                "sport": "FOOTBALL",
                "country": "ENGLAND",
                "state": "STARTED"
            }
      ];
};

module.exports.pollsData = function() {
  return [
          { 
              "_id" : "57c48c0fbbcb7c7c093852ad", 
              "eventId" : 1002916450, 
              "vote" : "Draw", 
              "dateTime" : "2016-08-29T19:25:03.925Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    },
              "__v" : 0
          },
          { 
              "_id" : "57c48c12bbcb7c7c093852ae", 
              "eventId" : 1002916451, 
              "vote" : "Away Team win", 
              "dateTime" : "2016-08-29T19:25:06.623Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    }, 
              "__v" : 0
          },
          { 
              "_id" : "57c48c15bbcb7c7c093852af", 
              "eventId" : 1003015194, 
              "vote" : "Home Team win", 
              "dateTime" : "2016-08-29T19:25:09.633Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    }, 
              "__v" : 0
          },
          { 
              "_id" : "57c48c18bbcb7c7c093852b0", 
              "eventId" : 1003022920, 
              "vote" : "Draw", 
              "dateTime" : "2016-08-29T19:25:12.570Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    }, 
              "__v" : 0
          },
          { 
              "_id" : "57c48c1bbbcb7c7c093852b1", 
              "eventId" : 1003026673, 
              "vote" : "Away Team win", 
              "dateTime" : "2016-08-29T19:25:15.341Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    }, 
              "__v" : 0
          }
      ];
};

module.exports.pollData = function() {
  return { 
              "_id" : "57c48c0fbbcb7c7c093852ad", 
              "eventId" : 1002916450, 
              "vote" : "Draw", 
              "dateTime" : "2016-08-29T19:25:03.925Z", 
              "voter" : 
                    { 
                        "id" : "57c414e9d5675f7d1a33f40b", 
                        "votername" : "cvnxch@test.com" 
                        
                    },
              "__v" : 0
          };
};

module.exports.newPollData = function() {
  return { 
            eventId : 1002916450, 
            vote : "Draw", 
            voter : 
                { 
                    id : "57c414e9d5675f7d1a33f40b", 
                    votername : "cvnxch@test.com" 
                }
        };
};