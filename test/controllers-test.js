var sinon = require('sinon');
//var assert = require('assert');
var expect = require('expect');
//var mongoose = require('mongoose');
//require('sinon-mongoose');
//var arrayDiff = require('lodash/array');

// Require db models so the all models will be declared
var SportsEvent = require("../models/sportsEvent");
var Poll = require("../models/poll");
require("../models/user");

//import sample data
var sampleData = require('./sample_data');

//import controllers
var Controller = require('../controllers/controller');

//import functions
var handler = require("../functions/index");

describe('Controllers', function(){
    describe('getPolls', function() {
        it('should exist', function(){
           
            expect(Controller.getPolls).toExist();
        });
        
        it('should redirect to sports events page when called', function() {
            var spy = expect.createSpy();
            var req = {params: {}};
            //var res = {redirect: sinon.stub()};
            var res = {redirect: spy};
            
            Controller.getPolls(req, res);
            
            //sinon.assert.calledWith(res.redirect, '/sportsevents');
            expect(spy).toHaveBeenCalledWith('/sportsevents');
        });
    });
    
    describe('getSportsEvents', function() {
        it('should exist', function () {
            expect(Controller.getSportsEvents).toExist();
        });
        
        it('should find all sports events and render to sports events page', function() {
            var SportsEventMock = sinon.mock(SportsEvent);
            var expectedResult = sampleData.sportsEventsData();
            
            SportsEventMock
                .expects('find').withArgs({})
                .yields(null, expectedResult);
                
            var req = {params: {}};
            var res = {render: sinon.stub()};
            
            Controller.getSportsEvents(req, res);
            
            SportsEventMock.verify();
            SportsEventMock.restore();
            
            sinon.assert.calledWith(res.render, "sportsevents", {sportsevents: expectedResult});
        });
        
    });
    
    describe('postPoll', function() {
        it('should exist', function(){
            expect(Controller.postPoll).toExist();
        });
        
        it('should create new vote in the db', function() {
            var PollMock = sinon.mock(Poll);
            var body = {eventId: 1002916450, vote: "Draw"};
            var user = {_id: "57c414e9d5675f7d1a33f40b", username: "cvnxch@test.com"};
            var newPoll = sampleData.newPollData();
            
            PollMock
                .expects('create').once().withArgs(newPoll);
                
            var req = {body, user};
            var res = {redirect: sinon.stub()};
            
            Controller.postPoll(req, res);
            
            PollMock.verify();
            PollMock.restore();
            
            sinon.assert.calledWith(res.redirect, "/polls/new");
        });
    });
    
    describe('getRandPoll', function() {
        it('should exist', function(){
            expect(Controller.getRandPoll).toExist();
        });
        
        it('should get all voted events ids for a user (when vFlag !== 1)', function() {
            var poll = sinon.spy(Poll, 'find');
            //var Handler = sinon.spy(handler, 'unVotedEventsHandler');
            
            var req = {session:{vFlag: 2}, user:{username:'cvnxch@test.com'}};
            var res = {};
            
            Controller.getRandPoll(req, res);
            
            poll.restore();
            //Handler.restore();
            
            sinon.assert.calledOnce(poll);
           // sinon.assert.calledOnce(Handler);
            expect(req.session.vFlag).toBe(1);
        });
        
        it('should call unVotedEventsHandler function when vflag is not 1');
        it('should call randomEventsHandler function when vflag is 1');
        
    });
})