var app = require("../app");
var supertest = require('supertest');
var agent     = supertest.agent(app);

//var chai = require("chai");
//var    expect = chai.expect;


describe('Routes', function(){
    describe('login', function() {
        it('should render login form', function(done){
            var url = '/login';
            agent
                .get(url)
                .expect('Content-Type', 'text/html; charset=utf-8')
                .expect(200, done);
                
        
        });
    });
    
    describe('signup', function() {
        it('should render signup form', function(done){
            var url = '/signup';
            agent
                .get(url)
                .expect('Content-Type', 'text/html; charset=utf-8')
                .expect(200, done);
                
        
        });
    });
    
    describe('sportsevents', function() {
        it('should render sports events page', function(done){
            var url = '/sportsevents';
            agent
                .get(url)
                .expect('Content-Type', 'text/html; charset=utf-8')
                .expect(200, done);
                
        
        });
    });
    
    describe('request to /', function() {
        it("redirects to sportsevents page", function(done){
            var url = '/';
            agent
                .get(url)
                .expect('Location', '/sportsevents')
                .expect(302, done);
        });
    });
    
    describe('request to polls route', function() {
        it("redirects to sportsevents page", function(done){
            var url = '/polls';
            agent
                .get(url)
                .expect('Location', '/sportsevents')
                .expect(302, done);
        });
    });
    
    describe("unauthenticated request to voting page", function() {
        var url = '/polls/new';
        
        it("redirects user to login page", function(done){
            agent
                .get(url)
                .expect('Location', '/login')
                .expect(302, done);
        });
    });
    
    /*describe('unauthenticated request to login', function() {
        it('redirects to login page', function(done){
            agent
              .post('/login')
              .type('form')
              .send({ username: 'lag@test.com' })
              .send({ password: 'lags' })
              .expect('Location', '/login')
              .expect(302, done);
      
        });
    });*/
    
});